﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using Android.Graphics;
using Android.Renderscripts;
using Android.Util;
using Android.Widget;
using Com.Tomergoldst.Tooltips;
using TooltipSample.Controls;
using TooltipSample.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTooltip), typeof(CustomTooltipRenderer))]
namespace TooltipSample.Droid.Renderers
{
    public class CustomTooltipRenderer : ViewRenderer<CustomTooltip, TooltipCustom>
    {
        TooltipCustom tooltipCustom;
    
        private readonly Context _context;
        Bitmap bitmap = null;
        public CustomTooltipRenderer(Context context) : base(context)
        {
            // _context = context;
            //  _toolTipsManager = new ToolTipsManager(this);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CustomTooltip> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
              
                // Subscribe
            }

            if (e.OldElement != null)
            {             
                // Unsubscribe
            }

            if (Control != null)
            {
                
                tooltipCustom = new TooltipCustom(Context, ViewGroup, Control, "Tooltip Control null");
                tooltipCustom.setTooltip();
                SetNativeControl(tooltipCustom);
                //SetNativeControl(Control);

            }

           if (Control == null)
            {
                
                tooltipCustom = new TooltipCustom(Context, ViewGroup, Control, "Tooltip Control");
                tooltipCustom.setTooltip();
                SetNativeControl(tooltipCustom);

            }
            UpdateBitmap(e.OldElement);
            tooltipCustom.SetImageBitmap(bitmap);

        }
        private async void UpdateBitmap(Image previous = null)
        {
            
            ImageSource source = Element.Source;
            if (previous == null || !object.Equals(previous.Source, Element.Source))
            {

                // I'm not sure where this comes from.
                Control.SetImageResource(17170445);
                if (source != null)
                {
                    try
                    {
                        bitmap = await GetImageFromImageSource(source, Context);
                    }
                    catch (TaskCanceledException)
                    {
                    }
                    catch (IOException)
                    {
                    }
                    catch (NotImplementedException)
                    {
                    }
                }
                if (Element != null && object.Equals(Element.Source, source))
                {
                  
                }
            }
            
        }
        private async Task<Bitmap> GetImageFromImageSource(ImageSource imageSource, Context context)
        {
            IImageSourceHandler handler;

            if (imageSource is FileImageSource)
            {
                handler = new FileImageSourceHandler();
            }
            else if (imageSource is StreamImageSource)
            {
                handler = new StreamImagesourceHandler(); // sic
            }
            else if (imageSource is UriImageSource)
            {
                handler = new ImageLoaderSourceHandler(); // sic
            }
            else
            {
                throw new NotImplementedException();
            }

            var originalBitmap = await handler.LoadImageAsync(imageSource, context);

            var  newtooltip = await Task.Run(() => CreateTooltipImage(originalBitmap, 25));

            return newtooltip;
        }
        private Bitmap CreateTooltipImage(Bitmap originalBitmap, int radius)
        {
            // Create another bitmap that will hold the results of the filter.
            Bitmap tooltipBitmap;
            tooltipBitmap = Bitmap.CreateBitmap(originalBitmap);

            // Create the Renderscript instance that will do the work.
            RenderScript rs = RenderScript.Create(Context);

            // Allocate memory for Renderscript to work with
            Allocation input = Allocation.CreateFromBitmap(rs, originalBitmap, Allocation.MipmapControl.MipmapFull, AllocationUsage.Script);
            Allocation output = Allocation.CreateTyped(rs, input.Type);

            // Load up an instance of the specific script that we want to use.
            ScriptIntrinsicBlur script = ScriptIntrinsicBlur.Create(rs, Android.Renderscripts.Element.U8_4(rs));
            script.SetInput(input);

            // Set the blur radius
            script.SetRadius(radius);

            // Start Renderscript working.
            script.ForEach(output);

            // Copy the output to the blurred bitmap
            output.CopyTo(tooltipBitmap);

            return tooltipBitmap;
        }
    }

}

