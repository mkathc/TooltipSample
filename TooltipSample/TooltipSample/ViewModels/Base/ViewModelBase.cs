﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace TooltipSample.ViewModels.Base
{
    public abstract class ViewModelBase : ExtendedBindableObject
    {
       // protected readonly IDialogService DialogService;
       // protected readonly INavigationService NavigationService;

        private bool _isBusy;
        private bool _isSync;
        private bool _isConsulting = false;
        private bool _isConnected = true;

       // public ICommand TryConnectCommand => new Command(async () => await TryConnectAsync());

       /* private async Task TryConnectAsync()
        {
            if (_isConsulting) { return; }

            try
            {
                _isConsulting = true;
                IsConnected = await ConnectivityHelper.CheckConnectivityAsync();
            }
            catch
            {

            }
            finally
            {
                _isConsulting = false;
            }
        }*/

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                RaisePropertyChanged(() => IsConnected);
            }
        }

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public bool IsSync
        {
            get
            {
                return _isSync;
            }

            set
            {
                _isSync = value;
                RaisePropertyChanged(() => IsSync);
            }
        }

        public ViewModelBase()
        {
            //Se agrega valores a las variables en el constructor
            //DialogService = ViewModelLocator.Resolve<IDialogService>();
            //NavigationService = ViewModelLocator.Resolve<INavigationService>();
            IsConnected = true;
            _isConnected = true;
            //GlobalSetting.Instance.BaseEndpoint = Settings.UrlBase;
        }

        public virtual async Task InitializeAsync(object navigationData)
        {
            IsConnected = true;
           // Plugin.Connectivity.CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
               // IsConnected = (await ConnectivityHelper.CheckConnectivityAsync());
            };

           // ConnectivityHelper.OnSyncStarted += ConnectivityHelper_OnSyncStarted;
          //  ConnectivityHelper.OnSyncFinished += ConnectivityHelper_OnSyncFinished;
            IsConnected = true;
           // IsConnected = await ConnectivityHelper.CheckConnectivityAsync();
        }

        private void ConnectivityHelper_OnSyncFinished(object sender, System.EventArgs e)
        {
            IsSync = false;
        }

        private void ConnectivityHelper_OnSyncStarted(object sender, System.EventArgs e)
        {
            IsSync = true;
        }
    }
}
