using System;
using TooltipSample.ViewModels.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace TooltipSample
{
	public partial class App : Application
	{

        public bool UseMockServices { get; set; }
        public App ()
		{
			InitializeComponent();
           // InitApp();
			MainPage = new MainPage();
		}
        private void InitApp()
        {
            UseMockServices = true;
            ViewModelLocator.RegisterDependencies(UseMockServices);
        }
        protected override void OnStart ()
		{
            base.OnStart();
            // Handle when your app starts
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
